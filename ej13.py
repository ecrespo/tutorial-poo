#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
http://jesusenlanet.blogspot.com/2016/02/clases-abstractas-con-python.html
http://www.3engine.net/wp/2015/02/clases-abstractas-en-python/
http://magmax.org/blog/2013/9/30/python-avanzado/
http://crysol.github.io/recipe/2007-06-27/ah-va-la-virgen-metaclases-con-python.html#.WBp80XUxDeT
http://www.voidspace.org.uk/python/articles/metaclasses.shtml
http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html
https://docs.python.org/2/library/abc.html
Con el decorador @abstractproperty podemos crear propiedades abstractas.
"""

from math import sqrt
from abc import ABCMeta, abstractmethod,abstractproperty

#Interface
class Figura(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def area(self):
        """Calcula el area"""
        pass

    @abstractmethod
    def perimetro(self):
        """Calcula el perimetro"""
        pass

    @abstractmethod
    def quienEresTu(self):

        """Devuelve el mensaje de quien es el objeto"""
        print (u"Soy una forma y un método abstracto ")

    @abstractproperty
    def color(self):
        pass

    @property
    def color(self):
        pass

    @color.setter
    def color(self,valor):
        pass

    @classmethod
    def __subclasshook__(cls, C):
        return NotImplemented


class Rectangulo(Figura):
    def __init__(self,ancho, alto):
        """Define los atributos de rectangulo"""
        self.ancho, self.alto = ancho, alto
        super(Rectangulo,self).__init__()
        self.__color = "rojo"

    def area(self):
        """Devuelve el area"""
        return self.ancho*self.alto

    def perimetro(self):
        """devuelve el perimetro"""
        return 2*self.ancho+2*self.alto

    @property
    def color(self):
        """Devuelve el atributo color"""
        return self.__color

    @color.setter
    def color(self,valor):
        """Define el color del rectangulo"""
        self.__color = valor

    def quienEresTu(self):
        """devuelve que es el objeto"""
        print (u"Soy un Rectangulo")


class Cuadrado(Rectangulo):

    def __init__(self,largo):
        """Define el largo del cuadrado y se lo pasa a la clase rectangulo"""
        self.largo = largo
        super(Cuadrado,self).__init__(largo,largo)

    def area(self):
        """DEvuelve el area del cuadrado"""
        return self.largo*self.largo



if __name__ == '__main__':
    r = Rectangulo(5,6)
    print ("Es una subclase rectangulo de shape?:",issubclass(Rectangulo,Figura))
    print ("Es una subclase cuadrado de shape?:",issubclass(Cuadrado,Figura))
    print ("Es una subclase cuadrado de rectangulo?:",issubclass(Cuadrado,Rectangulo))
    print ("Es una subclase rectangulo de cuadrado?:",issubclass(Rectangulo,Cuadrado))
    print ("r es una instancia de rectangulo?:",isinstance(r,Rectangulo))
    print ("r es una instancia de cuadrado?:",isinstance(r,Cuadrado))
    print ("Area del rectangulo:",r.area())
    print("Color del rectangulo:",r.color)
    r.color = "Black"
    print("Color del rectangulo:",r.color)
    print ("Perimetro del rectangulo",r.perimetro())
    r.quienEresTu()
    sq = Cuadrado(5)
    print ("Area del cuadrado:",sq.area())
    print ("Perimetro del cuadrado:",sq.perimetro())
