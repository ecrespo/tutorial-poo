#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import sqrt



class Punto2D(object):
    """Representacion de un punto en 2 dimensiones"""
    cont_puntos = 1

    def __init__(self,punto=(0,0)):
        '''constructor del punto'''
        self.__x,self.__y = punto
        Punto2D.cont_puntos += 1

    @property
    def ValorX(self):
        '''Devuelve el valor de X'''
        return self.__x

    @ValorX.setter
    def ValorX(self,x):
        '''Se le asigna un valor  a x por medio de setter'''
        self.__x = x

    @property
    def ValorY(self):
        '''Devuelve el valor de y'''
        return self.__y

    @ValorY.setter
    def ValorY(self,y):
        '''Se le asigna un valor a y por medio del setter'''
        self.__y = y

    @property
    def punto(self):
        """el getter de punto, devuelve el punto"""
        return (self.__x,self.__y)

    @punto.setter
    def punto(self,punto):
        ''''el setter de punto'''
        self.__x,self.__y = punto

    @property
    def cantPuntos(self):
        '''Devuelve la cantidad de puntos creados'''
        return Empleado.cont_puntos

    @punto.deleter
    def punto(self):
        '''deleter de punto'''
        del self.__x
        del self.__y
        cont_puntos = 0

    def mover(self,x,y):
        '''mueve el punto a un nuevo punto'''
        self.__x, self.__y = x,y

    def reset(self):
        '''coloca en el origen al punto'''
        self.mover(0,0)

    def distanciaOtroPunto(self,oPunto):
        '''devuelve la distancia entre el punto original y un punto dado'''
        return sqrt((self.__x - oPunto.punto[0])**2 + (self.__y - oPunto.punto[1])**2 )

    @staticmethod
    def autor(autor):
        '''Se define el autor de la clase'''
        return "El autor de esta clase es: {}".format(autor)

    @classmethod
    def cantidadPuntos(cls,cantidad):
        '''Se cambia la cantidad de puntos'''
        cls.cont_puntos = cantidad

if __name__ == "__main__":
    #Se crea cordenada de la clase Punto2D donde se le pasa (4,6)
    cordenada = Punto2D((4,6))
    #Se muestra el valor del punto.
    print(cordenada.punto)
    #Se asigna por setter un nuevo valor
    cordenada.punto = (10,15)
    #Se muestra el nuevo valor de cordenada
    print(cordenada.punto)
    #Se calcula la distancia con respecto a otro punto.
    print(cordenada.distanciaOtroPunto(Punto2D((100,100))))
    #Se fija el punto en un valor (0,0)
    cordenada.reset()
    #Se muestra el nuevo valor de la cordenada.
    print (cordenada.punto)
    #Se muestra un mensaje pasando el autor de la clase.
    print (cordenada.autor("Ernesto"))
    #Muestra la cantidad de puntos que se han creado.
    print ('Cantidad de puntos : {0}'.format(cordenada.cont_puntos ))
    #Se modifica la cantidad de puntos a 1 por medio de setter.
    Punto2D.cantidadPuntos(1)
    #Se vuelve a mostrar la cantida de puntos.
    print ('Cantidad de puntos : {0}'.format(cordenada.cont_puntos ))
    #Se borra la instancia cordenada.
    del(cordenada)
    #Se intenta mostrar el punto pero va a devolver que no se puede
    try:
        print (cordenada.punto)
    except (NameError):
        print ("No existe el objeto cordenada")
