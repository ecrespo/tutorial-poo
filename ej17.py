#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""multiple herencia"""


class ListaContactos(list):
    def buscar(self,nombre):
        '''Retorna todos los contactos que contengan el nombre'''
        contactos_encontrados = []
        for contacto in self:
            if nombre in contacto.nombre:
                contactos_encontrados.append(nombre)
        return contactos_encontrados

class Direccion(object):
    def __init__(self,calle, ciudad,estado, codigo):
        '''Inicializa la clase con la calle, ciudad, estado y codigo postal)'''
        self.calle = calle
        self.ciudad = ciudad
        self.estado = estado
        self.codigo = codigo

class Contacto(object):
    '''Clase contacto que guarda una lista de los contactos e inicializa con el nombre y el correo del contacto'''

    todos_contactos  = ListaContactos()

    def __init__(self,nombre, correo):
        self.nombre = nombre
        self.correo = correo
        Contacto.todos_contactos.append(self)

class Amigo(Contacto,Direccion):
    def __init__(self,nombre, correo, telefono,calle,ciudad,estado,codigo):
        Contacto.__init__(self,nombre,correo)
        self.telefono = telefono
        Direccion.__init__(self,calle,ciudad,estado,codigo)


if __name__ == "__main__":
    contacto = Amigo("Ernesto","seraph2@contacto.com","04155556565","paez","guacara","carabobo","2015")
    print(contacto.nombre,contacto.correo,contacto.telefono,contacto.calle,contacto.ciudad,contacto.estado,contacto.codigo)
