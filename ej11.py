#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod

class Enemy (object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def attackPlayer(self,player):
        pass

class EnvironmentAsset(object):
    __name__ = ABCMeta

    @abstractmethod
    def __init__(self):
        self.mobile = False

class Trap(Enemy,EnvironmentAsset):
    def __init__(self):
        super(Trap,self).__init()

    def attackPlayer(self,player):
        return player.health - 10


if __name__ == "__main__":
    x = Trap()
>>>>>>> 01e016db580621791ced01c2b3c031e3784acce3
