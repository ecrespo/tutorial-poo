# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

class Empleado:
    def __init__(self,nombre, apellido,email,sueldo):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.sueldo = sueldo
        
    def fullName(self):
        return '{} {}'.format(self.nombre,self.apellido)


emp1 = Empleado('Jane','Doe','janedoe@doe.com',4599)
emp2 = Empleado('John','Doe','johndoe@doe.com',5599)


print (emp1.fullName())

print (emp2.fullName())
print (Empleado.fullName(emp1))