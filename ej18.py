#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""multiple herencia, problema del diamante"""


class A(object):
    def __init__(self,mensajeA):
        self.mensajeA = mensajeA


    @staticmethod
    def quienSoy():
        return "Soy A"

class B(A):
    def __init__(self,mensajeA, mensajeB):
        A.__init__(self,mensajeA)
        self.mensajeB = mensajeB


    @staticmethod
    def quienSoy():
        return "Soy B"

class C(A):
    def __init__(self,mensajeA,mensajeC):
        A.__init__(self,mensajeA)
        self.mensajeC = mensajeC


    @staticmethod
    def quienSoy():
        return "Soy C"

class D(B,C):
    def __init__(self,mensajeA,mensajeB,mensajeC,mensajeD):
        B.__init__(self,mensajeA,mensajeB)
        C.__init__(self,mensajeA,mensajeC)
        self.mensajeD = mensajeD

    @staticmethod
    def quienSoy():
        return "Soy D"

if __name__ == '__main__':
    ca = A("prueba de A")
    cb = B("prueba de A desde B","prueba de B")
    cc = C("prueba de A desde C","prueba de C")
    cd = D("prueba de A desde D","prueba de B desde D","prueba de C desde D","prueba de D")
    print ("Mensaje de A:",ca.mensajeA)
    print ("Quien es?: ",ca.quienSoy())
    print ("Mensaje 1 de B:", cb.mensajeB)
    print("Mensaje 2 de B:", cb.mensajeA)
    print ("Quien es?:",cb.quienSoy())
    print ("Mensaje 1 de C:", cc.mensajeC)
    print ("Mensaje 2 de C:", cc.mensajeA)
    print ("Quien es?:", cc.quienSoy())
    print ("Mensaje 1 de D:", cd.mensajeD)
    print ("Mensaje 2 de D:", cd.mensajeB)
    print ("Mensaje 3 de D:", cd.mensajeC)
    print ("Mensaje 4 de D:", cd.mensajeA)
    print("Quien es?:", cd.quienSoy())
