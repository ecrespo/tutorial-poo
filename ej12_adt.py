#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
http://ldc.usb.ve/~gabro/teaching/CI2126/TADPilaLista.pdf
https://es.wikipedia.org/wiki/Tipo_de_dato_abstracto
http://crysol.github.io/recipe/2007-06-27/patrn-singleton-en-python-como-metaclase.html#.WBZgn3UxDeQ
http://crysol.github.io/recipe/2009-12-25/patrn-flyweight-en-python-como-metaclase.html#.WBZgl3UxDeQ
https://www.python.org/pycon/dc2004/papers/24/metaclasses-pycon.pdf
http://www.ibm.com/ve-es/
http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html
http://www.voidspace.org.uk/python/articles/metaclasses.shtml
http://crysol.github.io/recipe/2007-06-27/ah-va-la-virgen-metaclases-con-python.html#.WBZgpXUxDeQ
http://www.3engine.net/wp/2015/02/decoradores-python/
http://crysol.org/node/600
http://www.3engine.net/wp/2015/02/clases-abstractas-en-python/
http://jesusenlanet.blogspot.com/2016/02/clases-abstractas-con-python.html
http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
https://pythontips.com/2016/08/19/interesting-python-tutorials/
https://codequs.com/p/HJo2PAkgc/django-application-inside-a-docker-container/
https://github.com/dloss/python-pentest-tools
"""
from math import sqrt

from abc import ABCMeta, abstractmethod
class ADTPunto (object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def ValorX(self):
        pass

    @abstractmethod
    def ValorY(self):
        pass

    @abstractmethod
    def mover(self,puntoNuevo):
        pass

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def distanciaOtroPunto(self,punto):
        pass

class Punto(ADTPunto):
    def __init__(self,x,y):
        self.__x = x
        self.__y = y
        super(Punto,self).__init__()

    @property
    def punto(self):
        """el getter de punto, devuelve el punto"""
        return (self.__x,self.__y)

    @punto.setter
    def punto(self,Punto):
        """Asigna nuevo valor al punto"""
        self.__x = Punto.punto[0]
        self.__y = Punto.punto[1]

    @punto.deleter
    def punto(self):
        """Borra los valores del punto"""
        del self.__x
        del self.__y


    @property
    def ValorX(self):
        """Devuelve el valor de x"""
        return self.__x

    @property
    def ValorY(self):
        """Devuelve el valor de y"""
        return self.__y


    def reset(self):
        """Fija el punto en (0,0)"""
        self.__x = 0
        self.__y = 0

    def distanciaOtroPunto(self,oPunto):
        '''devuelve la distancia entre el punto original y un punto dado'''
        return sqrt((self.__x - oPunto.punto[0])**2 + (self.__y - oPunto.punto[1])**2 )

    def mover(self,oPunto):
        """Cambia el punto a un nuevo punto"""
        self.__x = oPunto.punto[0]
        self.__y = oPunto.punto[1]

if __name__ == "__main__":
    try:
        prueba = ADTPunto()
    except (TypeError):
        print ("No puede instanciar un tipo abstracto de datos")

    punto2d = Punto(3,5)
    print(punto2d.punto)
    print(punto2d.ValorX)
    print(punto2d.ValorY)
    punto2d.mover(Punto(5,5))
    print(punto2d.punto)
    print(punto2d.distanciaOtroPunto(Punto(10,10)))
    punto2d.reset()
    print(punto2d.punto)
