#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
http://jesusenlanet.blogspot.com/2016/02/clases-abstractas-con-python.html
http://www.3engine.net/wp/2015/02/clases-abstractas-en-python/
http://magmax.org/blog/2013/9/30/python-avanzado/
http://crysol.github.io/recipe/2007-06-27/ah-va-la-virgen-metaclases-con-python.html#.WBp80XUxDeT
http://www.voidspace.org.uk/python/articles/metaclasses.shtml
http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html

Con el decorador @abstractproperty podemos crear propiedades abstractas.
"""

from math import sqrt
from abc import ABCMeta, abstractmethod


class Titulo(str):
    def __new__(cls,val):
        print ("Construyendo un nuevo objeto")
        return str.__new__(cls, val.title())

class MiMetaclase(type):
    def __new__(meta,name,bases,dct):
        print ("Creando la metaclase ",name)
        return type.__new__(meta,name, bases,dct)

    def __init__(cls,name,bases, dct):
        print ("Inicializando la clase ", name)
        type.__init__(cls,name,bases,dct)


import types
class AutoNot(type):
    def __init__(cls,name,bases,dct):
        type.__init__(cls,name,bases,dct)
        methods = [x for x in dct if isinstance(dct[x], types.FunctionType)]
        for m in methods:
            setattr(cls, 'not_%s' % m, lambda self: not dct[m](self))



if __name__ == '__main__':
    Titulo("Transparencias, adios!")
    x = MiMetaclase('X',(),{})
    A = AutoNot('A', (), {'yes': lambda self:True})
    a = A()

    print (a.yes())
    print (a.not_yes())
