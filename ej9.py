#!/usr/bin/env python3
 # -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod

class BaseClass (object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def printHam(self):
        pass

class InClass(BaseClass):
    def printHam(self):
        print ("Ham")

if __name__ == "__main__":
    x = BaseClass()
    print (x)
    y = InClass()
    y.printHam()
