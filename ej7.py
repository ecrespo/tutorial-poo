#!/usr/bin/env python3.4
 # -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod

class BaseClass(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def printHam(self):
        pass

class InClass(BaseClass):
    def printHam(self):
        print ("ham")


if __name__ == "__main__":
    x = InClass()
    x.printHam()
