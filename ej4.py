#!/usr/bin/env python3.4
 # -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

class Empleado:
    numEmpleados = 0
    incremento = 1.04

    def __init__(self,nombre, apellido,email,sueldo):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.sueldo = sueldo

        Empleado.numEmpleados += 1

    def fullName(self):
        return '{} {}'.format(self.nombre,self.apellido)

    def apliqueAumento(self):
        self.sueldo = int (self.sueldo* self.incremento )

    @classmethod
    def asignaIncremento(cls, cantidad):
        cls.incremento = cantidad

    @classmethod
    def from_string(cls,empString):
        nombre, apellido, email, sueldo = empString.split("-")
        return cls(nombre,apellido,email,sueldo)

    @staticmethod
    def is_workday(day):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


class Desarrollador(Empleado):
    incremento = 1.1

    def __init__(self,nombre, apellido,email,sueldo,prog_lang):
        super().__init__(nombre,apellido,email,sueldo)
        #Empleado.__init__(self,nombre,apellido,email,sueldo)
        self.prog_lang = prog_lang


class Manager(Empleado):
    incremento = 1.5

    def __init__(self,nombre,apellido,email,sueldo,empleados=None):
        super().__init__(nombre,apellido,email,sueldo)
        #Empleado.__init__(self,nombre,apellido,email,sueldo)
        if empleados is None:
            self.empleados = []
        else:
            self.empleados = empleados


    def add_emp(self,emp):
        if emp not in self.empleados:
            self.empleados.append(emp)

    def remove_emp(self,emp):
        if emp in self.empleados:
            self.empleados.remove(emp)

    def print_emps(self):
        for emp in self.empleados:
            print('-->', emp.fullName())



dev1 = Desarrollador('Jane','Doe','janedoe@doe.com',4599,'Python')
dev2 = Desarrollador('John','Doe','johndoe@doe.com',5599,'Java')

mgr1 = Manager('Sue','Smith',90000,dev1)

print (isinstance(mgr1,Desarrollador))
print (issubclass(Desarrollador,Empleado))
mgr1.add_emp(dev2)

mgr1.add_emp(dev1)
mgr1.print_emps()
