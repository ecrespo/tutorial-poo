# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

class Empleado:
    numEmpleados = 0
    incremento = 1.04
    
    def __init__(self,nombre, apellido,email,sueldo):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.sueldo = sueldo
        
        Empleado.numEmpleados += 1
        
    def fullName(self):
        return '{} {}'.format(self.nombre,self.apellido)
        
    def apliqueAumento(self):
        self.sueldo = int (self.sueldo* self.incremento )


emp1 = Empleado('Jane','Doe','janedoe@doe.com',4599)
emp2 = Empleado('John','Doe','johndoe@doe.com',5599)


print (emp1.fullName())

print (emp2.fullName())
print (Empleado.fullName(emp1))

print (emp1.sueldo)
emp1.apliqueAumento()
print (emp1.sueldo)
print (Empleado.incremento)
print (emp1.incremento)
print (emp2.incremento)

emp2.incremento = 1.1

print (emp2.__dict__)
print ("----")

print (Empleado.incremento)
print (emp1.incremento)
print (emp2.incremento)


print (emp1.__dict__)


print (Empleado.__dict__)

Empleado.incremento = 1.5
print (Empleado.incremento)
print (emp1.incremento)
print (emp2.incremento)
