#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Se importa la clase Punto2D de ej12.py
from ej12 import Punto2D
#Se importa raiz cuadrada
from math import sqrt

class Punto3D(Punto2D):
    '''Se define la clase Punto3D'''

    def __init__(self,punto3d):
        '''Método init donde se le asigna x y y a Punto2D por medio de super'''
        super(Punto3D,self).__init__(punto3d[0:-1])
        self.__z = punto3d[-1]

    @property
    def ValorZ(self):
        '''devuelve el valor de z por medio de getter'''
        return self.__z

    @ValorZ.setter
    def ValorZ(self,z):
        '''Se asigna un valor a z por medio de setter'''
        self.__z = z

    @ValorZ.deleter
    def ValorZ(self):
        '''se borra el valor de z por medio de deleter'''
        del (self.__z)

    @property
    def punto(self):
        '''el getter de punto, devuelve el punto'''
        x,y = super(Punto3D,self).punto
        return (x,y,self.ValorZ)

    @punto.setter
    def punto(self,punto3d):
        '''Se le asigna un valor al punto por medio de setter'''
        super(Punto3D,self).mover(punto3d[0],punto3d[1])
        self.__z = punto3d[-1]

    @punto.deleter
    def punto(self):
        '''Se borra el punto por medio de deleter'''
        print ("Punto borrado")
        del Punto2D
        del self.__z

    def mover(self,x,y,z):
        '''Se asigna un nuevo valor al punto'''
        super(Punto3D,self).mover(x,y)
        self.__z = z

    def reset(self):
        '''Se fija el punto como (0,0,0)'''
        super(Punto3D,self).mover(0,0)
        self.__z = 0

    def distanciaOtroPunto(self,oPunto):
        '''devuelve la distancia entre el punto original y un punto dado'''
        x,y = super(Punto3D,self).punto
        z = self.__z
        return sqrt((x - oPunto.punto[0])**2 + (y - oPunto.punto[1])**2 + (z - oPunto.ValorZ) ** 2 )




if __name__ == '__main__':
    #Se crea la instancia Punto3D con el punto.
    punto3d = Punto3D((4,6,4))
    #Se muestra el valor del punto
    print(punto3d.punto)
    #Se asigna un nuevo valor a punto.
    punto3d.punto = (3,3,3)
    #Se muestra dicho valor
    print(punto3d.punto)
    #Se muestra los valores de x,y y z.
    print(punto3d.ValorX)
    print(punto3d.ValorY)
    print(punto3d.ValorZ)
    #Se asigna el valor (0,0,0)
    punto3d.reset()
    #Se muestra su valor.
    print(punto3d.punto)
    #Se calcula la distancia con otro punto.
    print(punto3d.distanciaOtroPunto(Punto3D((15,45,55))))
    #Se borra el punto3d.
    del(punto3d)
    #Se intenta mostrar el valor o devuelve que el objeto no existe.
    try:
        print (punto3d.punto)
    except (NameError):
        print ("No existe el objeto punto3d")
