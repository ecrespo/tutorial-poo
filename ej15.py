#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Vehiculo(object):
    """Clase vehiculo"""
    def __init__(self,color,puertas,cauchos,tipovehiculo):
        """Constructor"""
        self.color = color
        self.puertas = puertas
        self.cauchos = cauchos
        self.tipovehiculo = tipovehiculo

    def frenar(self):
        """Detener el carro """
        return "frenando"

    def manejar(self):
        """manejar el carro"""
        return "Conduciendo"


class Carro(Vehiculo):
    def frenar(self):
        return "Clase carro frena lentamente"


if __name__ == "__main__":
    carro = Vehiculo("Azul",4,4,"carro")
    print(carro.frenar())
    camioneta = Vehiculo("Negra",5,4,"camioneta")
    print(camioneta.color)
    print(camioneta.puertas)
    car = Carro("Amarillo",4,4,"carro")
    print(car.frenar())
