#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Composición"""
class A(object):
    def a1(self):
        print("a1")

class B(object):
    def b(self):
        print ("b")
        A().a1()


if __name__ =="__main__":
    objetoB = B()
    objetoB.b()
