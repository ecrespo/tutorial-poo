# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

class Empleado:
    numEmpleados = 0
    incremento = 1.04
    
    def __init__(self,nombre, apellido,email,sueldo):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.sueldo = sueldo
        
        Empleado.numEmpleados += 1
        
    def fullName(self):
        return '{} {}'.format(self.nombre,self.apellido)
        
    def apliqueAumento(self):
        self.sueldo = int (self.sueldo* self.incremento )
        
    @classmethod
    def asignaIncremento(cls, cantidad):
        cls.incremento = cantidad
    
    @classmethod
    def from_string(cls,empString):
        nombre, apellido, email, sueldo = empString.split("-")
        return cls(nombre,apellido,email,sueldo)
    
    @staticmethod
    def is_workday(day):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True

emp1 = Empleado('Jane','Doe','janedoe@doe.com',4599)
emp2 = Empleado('John','Doe','johndoe@doe.com',5599)

Empleado.asignaIncremento(1.05)

print(emp1.incremento)
print(emp2.incremento)
print(Empleado.incremento)

nuevo_empleado = Empleado.from_string("Ernesto-Crespo-ecrespo@gmail.com-15000")

print (nuevo_empleado.__dict__)

import datetime

dia = datetime.date(2016,7,10)
print (Empleado.is_workday(dia))